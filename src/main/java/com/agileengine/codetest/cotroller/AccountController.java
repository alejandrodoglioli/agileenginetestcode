package com.agileengine.codetest.cotroller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.agileengine.codetest.model.Transaction;
import com.agileengine.codetest.service.AccountServiceImpl;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/v1")
@Api(value = "agileengineapi", description = "Accounts api rest developed by Alejandro Doglioli for agilengine")
public class AccountController {

	@Autowired
	private AccountServiceImpl accountService;

	@RequestMapping(value = "/accounts", method = RequestMethod.GET)
	@ApiOperation(value = "Get a list of all accounts", response = List.class)
	public ResponseEntity<List<Transaction>> getAllAccounts() {
		return new ResponseEntity(accountService.getAllAccounts(), HttpStatus.OK);
	}

}
