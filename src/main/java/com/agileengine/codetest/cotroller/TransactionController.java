package com.agileengine.codetest.cotroller;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.agileengine.codetest.exceptions.TransactionNotExistException;
import com.agileengine.codetest.model.Account;
import com.agileengine.codetest.model.Transaction;
import com.agileengine.codetest.model.TransactionType;
import com.agileengine.codetest.service.AccountServiceImpl;
import com.agileengine.codetest.service.TransactionServiceImpl;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/v1")
@Api(value = "agileengineapi", description = "Transactions api rest developed by Alejandro Doglioli for agilengine")
public class TransactionController {

	@Autowired
	private TransactionServiceImpl transactionService;

	@Autowired
	private AccountServiceImpl accountService;

	@RequestMapping(value = "/transactions", method = RequestMethod.GET)
	@ApiOperation(value = "Get a list of all transactions", response = List.class)
	public ResponseEntity<List<Transaction>> getAllTransactions() {
		return new ResponseEntity(transactionService.getAllTransactions(), HttpStatus.OK);
	}

	@RequestMapping(value = "/transactions/by_dates", method = RequestMethod.GET)
	@ApiOperation(value = "Get all transactions between 2 dates, by default take one month.", response = List.class)
	public ResponseEntity<List<Transaction>> getTransactionsBetweenDates(
			@RequestParam(name = "start_date", required = false) @DateTimeFormat(iso = ISO.DATE) LocalDate startDate,
			@RequestParam(name = "end_date", required = false) @DateTimeFormat(iso = ISO.DATE) LocalDate endDate) {

		if (startDate == null)
			startDate = LocalDate.now().minusMonths(1);

		if (endDate == null)
			endDate = startDate.minusDays(1);

		List<Transaction> transactions = transactionService.getTransactionBetweenDates(startDate, endDate);

		return new ResponseEntity<>(transactions, HttpStatus.OK);
	}

	@GetMapping(value = "/transactions/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ExceptionHandler(TransactionNotExistException.class)
	public ResponseEntity<Transaction> getTransaction(@PathVariable() Long id) {

		Optional<Transaction> transaction = transactionService.getTransaction(id);
		if (transaction.isPresent()) {
			return new ResponseEntity<Transaction>(transaction.get(), HttpStatus.OK);
		}

		return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
	}

	@RequestMapping(value = "/transactions/accountid/{accountId}", method = RequestMethod.POST)
	@ApiOperation(value = "Create new transaction.", response = List.class)
	public ResponseEntity<Transaction> addTransaction(@PathVariable Long accountId,
			@RequestBody @Valid Transaction transaction) {
		if (accountId == null)
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

		Optional<Account> account = accountService.getAccount(accountId);
		if (account.isPresent()) {
			Account accountToSave = account.get();
			if (transaction.getTransactionType().equals(TransactionType.CREDIT)) {
				accountToSave.setAmount(accountToSave.getAmount() + transaction.getAmount());
			} else {
				accountToSave.setAmount(accountToSave.getAmount() - transaction.getAmount());
			}
			accountService.updateAccount(accountToSave);
		}

		Transaction saved = transactionService.addTransaction(transaction);
		return new ResponseEntity<>(saved, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/transactions/{id}", method = RequestMethod.PUT)
	@ApiOperation(value = "Update existent transaction.")
	public void updateTransaction(@RequestBody Transaction transaction, @PathVariable Long id) {
		transactionService.updateTransaction(id, transaction);
	}

	@RequestMapping(value = "/transactions/{id}", method = RequestMethod.DELETE)
	@ApiOperation(value = "Delete or cancel transaction.", response = List.class)
	public ResponseEntity<Object> cancelTransaction(@PathVariable Long id) {

		boolean cancelled = transactionService.cancelTransaction(id);

		if (cancelled) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	}
}
