package com.agileengine.codetest.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NO_CONTENT)
public class TransactionNotExistException extends RuntimeException {
	public TransactionNotExistException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

}
