package com.agileengine.codetest.service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.agileengine.codetest.exceptions.TransactionNotExistException;
import com.agileengine.codetest.model.Account;
import com.agileengine.codetest.model.Transaction;
import com.agileengine.codetest.repository.AccountRepository;
import com.agileengine.codetest.repository.TransactionRepository;

@Service
public class TransactionServiceImpl implements TransactionService {

	@Autowired
	private TransactionRepository transactionRepository;

	@Autowired
	private AccountRepository accountRepository;

	@Override
	public List<Transaction> getAllTransactions() {
		return transactionRepository.findAll();
	}

	@Override
	public Optional<Transaction> getTransaction(Long id) {
		return transactionRepository.findById(id);

	}

	@Override
	@Transactional()
	public Transaction addTransaction(Transaction transaction) {

		Optional<Account> account = accountRepository.findById((long) 1);
		if (account.isPresent())
			transaction.setAccount(account.get());
		return transactionRepository.save(transaction);
	}

	@Override
	@Transactional()
	public void updateTransaction(Long id, Transaction transaction) {
		Transaction savedTransaction = transactionRepository.getOne(transaction.getId());

		if (savedTransaction == null || savedTransaction.getActive().equals(false)) {
			String message = String.format("Transaction with id=%d not exist", transaction.getId());
			throw new TransactionNotExistException(message);
		}

		transactionRepository.save(transaction);
	}

	@Override
	@Transactional()
	public boolean cancelTransaction(Long id) {
		Transaction savedTransaction = transactionRepository.getOne(id);
		savedTransaction.setActive(false);

		transactionRepository.save(savedTransaction);

		return !savedTransaction.getActive();
	}

	@Override
	@Transactional(readOnly = true)
	public List<Transaction> getTransactionBetweenDates(LocalDate startDate, LocalDate endDate) {

		return transactionRepository.findAll().stream()
				.filter(x -> (startDate.isBefore(x.getEffectiveDate()) && endDate.isAfter(x.getEffectiveDate())))
				.collect(Collectors.toList());
	}
}
