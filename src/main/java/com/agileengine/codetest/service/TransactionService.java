package com.agileengine.codetest.service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import com.agileengine.codetest.model.Transaction;

public interface TransactionService {

	public List<Transaction> getAllTransactions();

	public List<Transaction> getTransactionBetweenDates(LocalDate startDate, LocalDate endDate);

	public Optional<Transaction> getTransaction(Long id);

	public Transaction addTransaction(Transaction reservation);

	public void updateTransaction(Long id, Transaction reservation);

	public boolean cancelTransaction(Long id);
}
