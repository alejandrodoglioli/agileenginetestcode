package com.agileengine.codetest.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.agileengine.codetest.model.Account;
import com.agileengine.codetest.repository.AccountRepository;

@Service
public class AccountServiceImpl implements AccountService {

	@Autowired
	private AccountRepository accountRepository;

	@Override
	public List<Account> getAllAccounts() {
		return accountRepository.findAll();
	}

	@Override
	public Optional<Account> getAccount(Long id) {
		return accountRepository.findById(id);
	}

	@Override
	public void addAccount(Account account) {
		accountRepository.save(account);
	}

	@Override
	public void updateAccount(Account account) {
		accountRepository.save(account);
	}

	@Override
	public void deleteAccount(Long id) {
		accountRepository.deleteById(id);
	}
}
