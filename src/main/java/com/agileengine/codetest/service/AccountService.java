package com.agileengine.codetest.service;

import java.util.List;
import java.util.Optional;

import com.agileengine.codetest.model.Account;

public interface AccountService {

	public List<Account> getAllAccounts();

	public Optional<Account> getAccount(Long id);

	public void addAccount(Account account);

	public void updateAccount(Account account);

	public void deleteAccount(Long id);
}
