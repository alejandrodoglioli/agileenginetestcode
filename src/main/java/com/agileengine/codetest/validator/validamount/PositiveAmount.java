package com.agileengine.codetest.validator.validamount;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Target({ElementType.TYPE, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = {PositiveAmountValidator.class})
@Documented
public @interface PositiveAmount {
	String message() default "The amount mus be greater than 0.";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
}
