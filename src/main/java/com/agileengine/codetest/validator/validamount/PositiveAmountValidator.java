package com.agileengine.codetest.validator.validamount;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.agileengine.codetest.model.Transaction;

public class PositiveAmountValidator implements ConstraintValidator<PositiveAmount, Transaction> {

	@Override
	public void initialize(PositiveAmount constraintAnnotation) {

	}

	@Override
	public boolean isValid(Transaction transaction, ConstraintValidatorContext constraintValidatorContext) {
		if (transaction == null) {
			return true;
		}

		return transaction.getAmount() > 0;
	}
}
