package com.agileengine.codetest.initdata;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import com.agileengine.codetest.model.Account;
import com.agileengine.codetest.model.Transaction;
import com.agileengine.codetest.model.TransactionType;
import com.agileengine.codetest.model.User;
import com.agileengine.codetest.repository.AccountRepository;
import com.agileengine.codetest.repository.TransactionRepository;
import com.agileengine.codetest.repository.UserRepository;


@Component
public class DataLoader implements ApplicationRunner {

	private AccountRepository accountRepository;
	private UserRepository userRepository;
	private TransactionRepository transactionRepository;

	@Autowired
	public DataLoader(AccountRepository accountRepository, UserRepository userRepository,
			TransactionRepository transactionRepository) {
		this.accountRepository = accountRepository;
		this.userRepository = userRepository;
		this.transactionRepository = transactionRepository;
	}

	@Override
	public void run(ApplicationArguments args) throws Exception {
		User user = new User("Alejandro", "Doglioli", null, null);
		
		Account account = new Account(user, 0.0);
		double amount = 0;
		Transaction t1 = new Transaction(account, TransactionType.CREDIT, 1000.0, LocalDate.of(2020, 9, 3));
		amount += t1.getAmount();
		Transaction t2 = new Transaction(account, TransactionType.CREDIT, 2000.0, LocalDate.of(2020, 9, 3));
		amount += t2.getAmount();
		Transaction t3 = new Transaction(account, TransactionType.CREDIT, 13500.0, LocalDate.of(2020, 9, 3));
		amount += t3.getAmount();
		Transaction t4 = new Transaction(account, TransactionType.CREDIT, 500.0, LocalDate.of(2020, 9, 3));
		amount += t4.getAmount();
		Transaction t5 = new Transaction(account, TransactionType.CREDIT, 400.0, LocalDate.of(2020, 9, 3));
		amount += t5.getAmount();
		Transaction t6 = new Transaction(account, TransactionType.CREDIT, 6000.0, LocalDate.of(2020, 9, 3));
		amount += t6.getAmount();
		Transaction t7 = new Transaction(account, TransactionType.CREDIT, 3000.0, LocalDate.of(2020, 9, 3));
		amount += t7.getAmount();
		Transaction t8 = new Transaction(account, TransactionType.CREDIT, 2400.0, LocalDate.of(2020, 9, 3));
		amount += t8.getAmount();
		Transaction t9 = new Transaction(account, TransactionType.DEBIT, 1000.0, LocalDate.of(2020, 9, 3));
		amount -= t9.getAmount();
		Transaction t10 = new Transaction(account, TransactionType.DEBIT, 8555.0, LocalDate.of(2020, 9, 3));
		amount -= t10.getAmount();
		Transaction t11 = new Transaction(account, TransactionType.DEBIT, 2400.0, LocalDate.of(2020, 9, 3));
		amount -= t11.getAmount();
		
		account.getTransactions().add(t1);
		account.getTransactions().add(t2);
		account.getTransactions().add(t3);
		account.getTransactions().add(t4);
		account.getTransactions().add(t5);
		account.getTransactions().add(t6);
		account.getTransactions().add(t7);
		account.getTransactions().add(t8);
		account.getTransactions().add(t9);
		account.getTransactions().add(t10);
		account.getTransactions().add(t11);
		
		account.setAmount(amount);
		
		accountRepository.save(account);
	}
}
