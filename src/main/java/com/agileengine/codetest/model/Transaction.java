package com.agileengine.codetest.model;

import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

import com.agileengine.codetest.model.base.GenericModel;
import com.agileengine.codetest.validator.validamount.PositiveAmount;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "TRANSACTIONS")
@PositiveAmount
@JsonIgnoreProperties(ignoreUnknown = true)
public class Transaction extends GenericModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ManyToOne(cascade = CascadeType.ALL)
	@ApiModelProperty(notes = "The account id")
	@JsonIgnore
	private Account account;

	@Column(name = "transaction_type", nullable = false)
	@NotNull(message = "Transaction Type required")
	@ApiModelProperty(notes = "Transaction type")
	private TransactionType transactionType;

	@Column(name = "amount", nullable = false)
	@NotNull(message = "Amount required")
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	@ApiModelProperty(notes = "The amount")
	private Double amount;

	@Column(name = "effective_date", nullable = false)
	@NotNull(message = "Effective date required")
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	@ApiModelProperty(notes = "The effective date")
	private LocalDate effectiveDate;

	public Transaction() {
		super();
	}

	public Transaction(Account account,
			@NotNull(message = "Transaction Type date required") TransactionType transactionType,
			@NotNull(message = "Amount required") Double amount,
			@NotNull(message = "Effective date required") LocalDate effectiveDate) {
		super();
		this.account = account;
		this.transactionType = transactionType;
		this.amount = amount;
		this.effectiveDate = effectiveDate;
		this.setInsertValues();
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public TransactionType getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(TransactionType transactionType) {
		this.transactionType = transactionType;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public LocalDate getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(LocalDate effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((account == null) ? 0 : account.hashCode());
		result = prime * result + ((amount == null) ? 0 : amount.hashCode());
		result = prime * result + ((effectiveDate == null) ? 0 : effectiveDate.hashCode());
		result = prime * result + ((transactionType == null) ? 0 : transactionType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Transaction other = (Transaction) obj;
		if (account == null) {
			if (other.account != null)
				return false;
		} else if (!account.equals(other.account))
			return false;
		if (amount == null) {
			if (other.amount != null)
				return false;
		} else if (!amount.equals(other.amount))
			return false;
		if (effectiveDate == null) {
			if (other.effectiveDate != null)
				return false;
		} else if (!effectiveDate.equals(other.effectiveDate))
			return false;
		if (transactionType != other.transactionType)
			return false;
		return true;
	}

}
