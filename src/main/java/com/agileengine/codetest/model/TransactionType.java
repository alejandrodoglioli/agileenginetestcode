package com.agileengine.codetest.model;

public enum TransactionType {
	DEBIT, CREDIT
}
